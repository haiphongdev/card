<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Game;

/**
 * Class Card
 * @package App\Models
 * @version April 5, 2019, 6:54 am UTC
 *
 * @property integer status
 * @property string seri
 * @property string code
 * @property integer provider
 * @property string amount
 * @property string name
 * @property string game
 */
class Card extends Model
{
    use SoftDeletes;

    public $table = 'cards';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'status',
        'seri',
        'code',
        'provider',
        'amount',
        'name',
        'game_id',
        'declared_value',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'integer',
        'seri' => 'string',
        'code' => 'string',
        'provider' => 'string',
        'amount' => 'string',
        'name' => 'string',
        'game_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'code' => 'required',
        'seri' => 'required',
    ];

    public function game()
    {
        return $this->belongsTo('App\Models\Game');
    }
    
}
