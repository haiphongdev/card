<?php

namespace App\Repositories;

use App\Models\Card;
use App\Repositories\BaseRepository;

/**
 * Class CardRepository
 * @package App\Repositories
 * @version April 5, 2019, 6:54 am UTC
*/

class CardRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'status',
        'seri',
        'code',
        'provider',
        'amount',
        'name',
        'game_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Card::class;
    }

    public function search($filter){//var_dump($filter);die;
        $results = Card::with('game')->orderBy('created_at', 'desc');
        if(isset($filter['code'])){
            $code = $filter['code'];
            $results = $results->where('code', 'LIKE', "%".$filter['code']."%");
        }
        if(isset($filter['seri'])){
            $results = $results->where('seri', 'LIKE', "%".$filter['seri']."%");
        }
        if(isset($filter['provider'])){
            $results = $results->where('provider', $filter['provider']);
        }
         if(isset($filter['status'])){
            $results = $results->where('status', $filter['status']);
        }
      
        return $results->paginate(6);
    }
}
