<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCardRequest;
use App\Http\Requests\UpdateCardRequest;
use App\Http\Requests\SearchCardRequest;
use App\Models\Provider;

use App\Repositories\CardRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use DB;

class CardController extends AppBaseController
{
    /** @var  CardRepository */
    private $cardRepository;
    public function __construct(CardRepository $cardRepo)
    {
        $this->cardRepository = $cardRepo;
    }

    /**
     * Display a listing of the Card.
     *
     * @param Request $request
     *
     * @return Response
     */

    protected function getProductList(){
              return Provider::pluck( 'name','code')->toArray();

    }
    protected function getStatusList(){
        return array(
                    1=>"Thành công đúng mệnh giá",
                    2=>"Thành công sai mệnh giá",
                    3=>"Thẻ lỗi",
                    99=>"Thẻ chờ",
                    );
    }

    public function index(Request $request)
    {
        $filter = new SearchCardRequest();
        $cards = $this->cardRepository->search($filter->input());
        $provider_list = $this->getProductList();
        $status_list = $this->getStatusList();

        return view('cards.index',compact('filter','provider_list','status_list','cards'));
    }

    /**
     * Show the form for creating a new Card.
     *
     * @return Response
     */
    public function create()
    {
        $provider_list = $this->getProductList();
        $status_list = $this->getStatusList();

        return view('cards.create',compact('provider_list','status_list'));
    }

    /**
     * Store a newly created Card in storage.
     *
     * @param CreateCardRequest $request
     *
     * @return Response
     */
    public function store(CreateCardRequest $request)
    {
        $input = $request->all();
//echo "<pre>"; print_r($input); echo "</pre>";die;
        $card = $this->cardRepository->create($input);

        Flash::success('Card saved successfully.');

        return redirect(route('cards.index'));
    }

    /**
     * Display the specified Card.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $card = $this->cardRepository->find($id);

        if (empty($card)) {
            Flash::error('Card not found');

            return redirect(route('cards.index'));
        }

        return view('cards.show')->with('card', $card);
    }

    /**
     * Show the form for editing the specified Card.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $card = $this->cardRepository->find($id);
        $provider_list = $this->getProductList();
        $status_list = $this->getStatusList();

        if (empty($card)) {
            Flash::error('Card not found');

            return redirect(route('cards.index'));
        }
        return view('cards.edit',compact('provider_list','status_list','card'));

    }

    /**
     * Update the specified Card in storage.
     *
     * @param int $id
     * @param UpdateCardRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCardRequest $request)
    {
        $card = $this->cardRepository->find($id);

        if (empty($card)) {
            Flash::error('Card not found');

            return redirect(route('cards.index'));
        }

        $card = $this->cardRepository->update($request->all(), $id);

        Flash::success('Card updated successfully.');

        return redirect(route('cards.index'));
    }

    /**
     * Remove the specified Card from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $card = $this->cardRepository->find($id);

        if (empty($card)) {
            Flash::error('Card not found');

            return redirect(route('cards.index'));
        }

        $this->cardRepository->delete($id);

        Flash::success('Card deleted successfully.');

        return redirect(route('cards.index'));
    }

    public function search(Request $filter)
    {
        $input = $filter->except(['refresh']);
        $provider_list = $this->getProductList();
        $status_list = $this->getStatusList();

        $cards = $this->cardRepository->search($input);
        return view('cards.index',compact('filter','provider_list','status_list'))
            ->with('cards', $cards);
    }
}
