<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGameRequest;
use App\Http\Requests\UpdateGameRequest;
use App\Models\Game;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use Carbon\Carbon;
//use DB;
use Intervention\Image\Facades\Image;

class GameController extends AppBaseController
{
    /** @var  GameRepository */

    public function __construct()
    {
    }

    /**
     * Display a listing of the Game.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $games =  Game::orderBy('order', 'desc')->get();
        return view('games.index')
            ->with('games', $games);
    }

    /**
     * Show the form for creating a new Game.
     *
     * @return Response
     */
    public function create()
    {
        return view('games.create');
    }

    /**
     * Store a newly created Game in storage.
     *
     * @param CreateGameRequest $request
     *
     * @return Response
     */
    public function store(CreateGameRequest $request)
    {
        $input = $request->all();
        $input['created_at'] = Carbon::now(); 
        $input['updated_at'] = Carbon::now(); 
        //$input['order'] =  DB::table('games')->max('order') +1 ;
        if($request->hasFile('icon')){
            $image = $request->file('icon');
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $location = public_path('icon'). '/' . $image_full_name;
            Image::make($image)->resize(20,20)->save($location);
            $input['icon'] = $image_full_name;
        }else {
        }
        $game = Game::create($input);

        Flash::success('Game saved successfully.');

        return redirect(route('games.index'));
    }

    /**
     * Display the specified Game.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $game = Game::findOrFail($id);

        if (empty($game)) {
            Flash::error('Game not found');

            return redirect(route('games.index'));
        }

        return view('games.show')->with('game', $game);
    }

    /**
     * Show the form for editing the specified Game.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $game = Game::findOrFail($id);

        if (empty($game)) {
            Flash::error('Game not found');

            return redirect(route('games.index'));
        }

        return view('games.edit')->with('game', $game);
    }

    /**
     * Update the specified Game in storage.
     *
     * @param int $id
     * @param UpdateGameRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGameRequest $request)
    {
        $input = $request->all();
        $game = Game::findOrFail($id);

        if (empty($game)) {
            Flash::error('Game not found');

            return redirect(route('games.index'));
        }
        //update icon image
        if($request->hasFile('icon')){
            $image = $request->file('icon');
                  // Delete the old picture

            $update_path = public_path('icon'). '/' .$game->icon;
            if($game->icon != null)
            {
                @unlink($update_path);
            }
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $location = public_path('icon'). '/' . $image_full_name;
            Image::make($image)->resize(20,20)->save($location);
            $input['icon'] = $image_full_name;
        }
        else {
        }
        $game->update($input);

        Flash::success('Game updated successfully.');

        return redirect(route('games.index'));
    }

    /**
     * Remove the specified Game from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $game = Game::findOrFail($id);

        if (empty($game)) {
            Flash::error('Game not found');

            return redirect(route('games.index'));
        }

        Game::delete($id);

        Flash::success('Game deleted successfully.');

        return redirect(route('games.index'));
    }
}
