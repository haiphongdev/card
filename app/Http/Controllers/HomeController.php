<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CardRepository;
use Flash;
use Illuminate\Support\Facades\Log;
use App\Models\Game;
use App\Models\Provider;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $cardRepository;
    public function __construct(CardRepository $cardRepo)
    {
        $this->cardRepository = $cardRepo;
    }
    protected function prepareRateProvider(){
        return Provider::pluck( 'rate','code')->toArray();
        
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $games = Game::orderBy('order', 'asc')->get();
        $providers = Provider::all();
        $providerRates  = $this->prepareRateProvider();
        $providerRates =  json_encode($providerRates, JSON_UNESCAPED_SLASHES );
        return view('frontend.index',compact('games','providers','providerRates'));
    }

    public function sendApi(Request $request )
    {

        $input=  $request->all();
        $input['status'] = 99;
        $card = $this->cardRepository->create($input);
        $url = 'http://napcuoc.net/chargingws/v2';
        $partner_id = '0601968451';
        $partner_key = '6dd372151552c79c1fbabc49d02829f4';

        $dataPost = array();
        $dataPost['partner_id'] = $partner_id;
        $dataPost['request_id'] = $card->id;
        $dataPost['telco'] = $request->provider;
        $dataPost['amount'] = $request->amount;
        $dataPost['serial'] = $request->seri;
        $dataPost['code'] = $request->code;
        $dataPost['command'] = 'charging';  // Nap the
        $sign = $this->creatSign($partner_key, $dataPost);
        $dataPost['sign'] = $sign;

        $data = http_build_query($dataPost);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        curl_setopt($ch, CURLOPT_REFERER, $actual_link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        $obj = json_decode($result);
        if(is_null($obj)){
            Flash::success("Đã có lỗi xảy ra");
        }else{
            Flash::success($obj->message);
        }
        return redirect(route('home'));
    }

    public function callback(Request $request)
    {
        //Log::info( print_r($request->all()));
        if(isset($request->callback_sign)) {
            $data = array('status'=>$request->status,);
            $providerRates  = $this->prepareRateProvider();
            if($request->status ==2 ){
                $provider = $request->telco;
                $rate = $providerRates[$provider];
                $data[ 'declared_value'] =  $request->value * $rate /100;
            }
            
            $card = Card::where('id',$request->request_id)->where('status',99)->first();
            if (empty($card)) {
                return 'Fail.Card is not exist.';
            }else{
                $card->update($data);
            }
        }
        return 'OK';
    }

   
    protected  function creatSign($partner_key, $params)
    {
        $data = array();
        $data['request_id'] = $params['request_id'];
        $data['code'] = $params['code'];
        $data['partner_id'] = $params['partner_id'];
        $data['serial'] = $params['serial'];
        $data['telco'] = $params['telco'];
        $data['command'] = $params['command'];
        ksort($data);
        $sign = $partner_key;
        foreach ($data as $item) {
            $sign .= $item;
        }
        
        return md5($sign);
    }
}
