<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::group(['middleware'=>'auth'], function() {
	//Route::resource('cards', 'CardController');
	Route::get('cards', 'CardController@index')->name('cards.index');
	Route::get('cards/create', 'CardController@create')->name('cards.create');
	Route::post('cards', 'CardController@store')->name('cards.store');
	Route::get('cards/{id}/edit', 'CardController@edit')->name('cards.edit');
	Route::put('cards/{id}', 'CardController@update')->name('cards.update');
	Route::delete('cards/{id}/delete', 'CardController@destroy')->name('cards.destroy');
	Route::get('cards/{id}/show', 'CardController@edit')->name('cards.show');
	Route::get('cards/search', 'CardController@search')->name('cards.search');
	Route::resource('games', 'GameController');
	Route::resource('providers', 'ProviderController');
});


Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/sendApi', 'HomeController@sendApi')->name('sendApi');
Route::post('/callback', 'HomeController@callback')->name('callback');


