@extends('layouts.frontend')
@section('content')
	<div class="row" style="margin-top: 50px;">
        <div class="col-md-8" style="float:none;margin:0 auto;">
            @include('flash::message')
            <form    action="{{url('/sendApi')}}" method="post">
				 {{csrf_field()}}
                <div class="form-group">
                    <label>Loại thẻ:</label>
                    <select id="provider" class="form-control" name="provider">
                        <option value="">Chọn loại thẻ</option>
                         @foreach($providers  as   $provider)
                        <option value="{{$provider->code}}">{{$provider->name}}</option>
                         @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Mệnh giá:</label>
                    <select id="price" class="form-control" name="amount">
                        <option value="">Chọn mệnh giá</option>
                        <option value="10000">10.000</option>
                        <option value="20000">20.000</option>
                        <option value="30000">30.000</option>
                        <option value="50000">50.000</option>
                        <option value="100000">100.000</option>
                        <option value="200000">200.000</option>
                        <option value="300000">300.000</option>
                        <option value="500000">500.000</option>
                        <option value="1000000">1.000.000</option>
                    </select>
                </div>

                 <div class="form-group">
                    <label>Giá trị thực:</label>
                    <input id ="realValue" type="text" class="form-control" name="declared_value" readonly/>
                </div>
                <div class="form-group">
                    <label>Số seri:</label>
                    <input type="text" class="form-control" name="seri"/>
                </div>
                <div class="form-group">
                    <label>Mã thẻ:</label>
                    <input type="text" class="form-control" name="code"/>
                </div>
                 <div class="form-group">
                    <label>Tên nhân vật:</label>
                    <input type="text" class="form-control" name="name"/>
                </div>
                 <div class="form-group">
                    <label>Tên game:</label>
                    <select class="form-control selectpicker" name="game_id">
                        @foreach($games as $game)
                        <option value="{{$game->id}}"  data-thumbnail="{{ asset('icon/'.$game->icon) }}">{{$game->name}}</option>
                        @endforeach
                    </select>
                </div>
                
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block" name="submit">NẠP NGAY</button>
                </div>
            </form>
        </div>
    </div>
   
@endsection

