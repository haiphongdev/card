<table class="table table-responsive" id="games-table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Order</th>
            <th>Icon</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($games as $game)
        <tr>
            <td>{!! $game->name !!}</td>
            <td>{!! $game->order !!}</td>
            <td><img src="{{ asset('icon/'.$game->icon) }}"/></td>
            <td>
                {!! Form::open(['route' => ['games.destroy', $game->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('games.show', [$game->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('games.edit', [$game->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>