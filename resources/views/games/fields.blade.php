<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Position Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order', 'Position:') !!}
    {!! Form::number('order', null, ['class' => 'form-control']) !!}
</div>

<!-- Icon Field -->
<div class="form-group col-sm-6">
      <label for="inputEmail3" class="col-sm-3 control-label">Icon</label>
      <div class="col-sm-8">
            <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="input-group input-large">
            <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
              <i class="fa fa-file fileinput-exists"></i>&nbsp;
              <span class="fileinput-filename"> </span>
          </div>
          <span class="input-group-addon btn default btn-file">
          <span class="fileinput-new bold uppercase">
           <i class="fa fa-picture-o"></i> Select icon </span>
<!--            <span class="fileinput-exists bold uppercase"> Change </span>
 -->           <input type="file" name="icon" > </span>
       <!--     <a href="javascript:;" class="input-group-addon btn red fileinput-exists bold uppercase" data-dismiss="fileinput">Remove</a> -->
            </div>
        </div>

          
        </div>
    </div><br>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('games.index') !!}" class="btn btn-default">Cancel</a>
</div>
