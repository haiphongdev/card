<div class="form-group col-sm-6">
    {!! Form::label('status', 'Trạng thái:') !!}
    {!!Form::select('status', $status_list, null, ['class' => 'form-control'])!!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('code', 'Mã thẻ:') !!} 
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('seri', 'Seri :') !!}
    {!! Form::text('seri', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('provider', 'Nhà mạng :') !!}
    {!!Form::select('provider', $provider_list, null, ['class' => 'form-control'])!!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Số tiền:') !!}  

    {!! Form::text('amount', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('declared_value', 'Số tiền thực:') !!}  

    {!! Form::text('declared_value', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('name', 'Tên nhân vật :') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('game', 'Tên game:') !!} 
    {!! Form::text('game_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('cards.index') !!}" class="btn btn-default">Cancel</a>
</div>
