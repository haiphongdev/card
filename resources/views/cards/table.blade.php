<table class="table table-responsive" id="cards-table">
    <thead>
        <tr>
            <th>Status</th>
            <th>Seri</th>
            <th>Code</th>
            <th>Provider</th>
            <th>Amount</th>
            <th>Real Amount</th>
            <th>Name</th>
            <th>Game</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($cards as $card)
     <?php 
     if($card->status==3){
         $label = "danger";
     }elseif($card->status==99) {
         $label = "warning";
     }else{
         $label = "success";
     } 
     ?>   
        <tr>
            <td>
                <span class="label label-{{$label}}">
                    {{$status_list[$card->status]}}
                </span>
            </td>
            <td>{!! $card->seri !!}</td>
            <td>{!! $card->code !!}</td>
            <td>{{ $provider_list[$card->provider] }}</td>
            <td>{!! $card->amount !!}</td>
            <td>{!! $card->declared_value !!}</td>
            <td>{!! $card->name !!}</td>
            <td>{!! $card->game->name !!}</td>
            <td>
                {!! Form::open(['route' => ['cards.destroy', $card->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('cards.show', [$card->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('cards.edit', [$card->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
          

</table>

<div class="fixed-table-pagination">
    <div class="pull-right pagination">
      {!! $cards->links() !!}
    </div>
</div>