{!! Form::model($filter, ['route' => ['cards.search'], 'method' => 'get']) !!}


<div class="form-group col-sm-3">
	<select name="provider" class="form-control"> 
		<option value="">Nhà mạng  </option>
		@foreach($provider_list as $key => $value)
		<option value="{{$key}}" {{ ($key == $filter->provider) ? 'selected' : '' }} >{{$value}}  </option>
		@endforeach

	</select>
</div>
<div class="form-group col-sm-3">
	<select name="status" class="form-control"> 
		<option value="">Trạng thái </option>
		@foreach($status_list as $key => $value)
		<option value="{{$key}}" {{ ($key == $filter->status) ? 'selected' : '' }} >{{$value}}  </option>
		@endforeach
	</select>
</div>
<div class="columns btn-group pull-right">
	<button type ="submit" class="btn btn-default" type="button" name="refresh" title="Refresh"><i class="glyphicon glyphicon-refresh icon-refresh"></i></button>
	<!-- <button class="btn btn-default" type="button" name="toggle" title="Toggle"><i class="glyphicon glyphicon glyphicon-list-alt icon-list-alt"></i></button>
	<div class="keep-open btn-group" title="Columns">
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-th icon-th"></i> <span class="caret"></span>
		</button>
		<ul class="dropdown-menu" role="menu">
			<li><label><input type="checkbox" data-field="title" value="1" checked="checked"> News Title</label></li><li><label><input type="checkbox" data-field="Category" value="2" checked="checked"> News Category</label></li><li><label><input type="checkbox" data-field="Details" value="3" checked="checked"> News Details</label></li><li><label><input type="checkbox" data-field="Status" value="4" checked="checked"> Status </label></li><li><label><input type="checkbox" data-field="Actions" value="5" checked="checked"> Actions</label></li></ul>
	</div> -->
</div>
<div class="pull-right search">
	<input name ="code" class="form-control" type="text" placeholder="Code" value="{{$filter->code}}">
</div>
<div class="pull-right search">
	<input name="seri" class="form-control" type="text" placeholder="Seri" value="{{$filter->seri}}">
</div>
 {!! Form::close() !!}
