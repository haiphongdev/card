<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $card->id !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $card->status !!}</p>
</div>

<!-- Seri Field -->
<div class="form-group">
    {!! Form::label('seri', 'Seri:') !!}
    <p>{!! $card->seri !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $card->code !!}</p>
</div>

<!-- Provider Field -->
<div class="form-group">
    {!! Form::label('provider', 'Provider:') !!}
    <p>{!! $card->provider !!}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $card->amount !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $card->name !!}</p>
</div>

<!-- Game Field -->
<div class="form-group">
    {!! Form::label('game', 'Game:') !!}
    <p>{!! $card->game !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $card->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $card->updated_at !!}</p>
</div>

