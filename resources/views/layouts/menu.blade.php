
<li class="{{ Request::is('cards*') ? 'active' : '' }}">
    <a href="{!! route('cards.index') !!}"><i class="fa fa-edit"></i><span>Cards</span></a>
</li>

<li class="{{ Request::is('games*') ? 'active' : '' }}">
    <a href="{!! route('games.index') !!}"><i class="fa fa-edit"></i><span>Games</span></a>
</li>

<li class="{{ Request::is('providers*') ? 'active' : '' }}">
    <a href="{!! route('providers.index') !!}"><i class="fa fa-edit"></i><span>Providers</span></a>
</li>

