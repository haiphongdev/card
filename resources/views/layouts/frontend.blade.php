<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Nạp thẻ</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
     <link rel="stylesheet" href="{{asset('css/bootstrap-select.css')}}"/>
          
</head>
<body>
    <header class="header-top">
    <div class="container">
        <div class="logo" style="margin-left: 200px">
            <a href="#">
                <img src="{{asset('images/logonapcuoc.png')}}" alt="">
            </a>
        </div>
        <style>
    @media (min-width: 768px) {
        .navigation ul > li:hover > ul {
            border-top: 2px solid #004b7f  !important;
        }
        a.hover_item:hover {
            color: #f1f1f1;
            background: #004b7f  !important;
        }
        .navigation > ul > li > a::before, .navigation > ul > li > span::before {
            background: #004b7f  !important;
        }
        .navigation ul > li ul:before{
            border-bottom: 4px dashed #004b7f  !important;
        }
    }


</style>
    </div>
</header>
    <div class="container">
        @yield('content')
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"
            ></script>
    <script src="{{asset('js/bootstrap-select.js')}}"></script>
    <script type="text/javascript">

$(document).ready(function() {
  // Initiate with custom caret icon
  $('select.selectpicker').selectpicker({
    caretIcon: 'glyphicon glyphicon-menu-down'
  });
});


    </script>
 <script type="text/javascript">
        var rateArr = {!! $providerRates !!};
       $('#provider').on('change', function() {
         var price =  $('#price').val();
         provider = this.value ;
         rate = rateArr[provider];
         var realValue = price*rate/100;
           $('#realValue').val(realValue); 
        });

       $('#price').on('change', function() {
         var price = this.value  
         provider =  $('#provider').val();
         rate = rateArr[provider];
         var realValue = price*rate/100;
           $('#realValue').val(realValue); 
        });
    </script>
</body>
</html>